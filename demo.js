//Create the scene
var scene = new THREE.Scene();
//Create the camera
var camera = new THREE.PerspectiveCamera(35, window.innerWidth / window.innerHeight, 1, 1000 );

//Sbybox
var cube = new THREE.CubeGeometry(100, 100, 100);
var skyboxMaterial = new THREE.MeshBasicMaterial({color: 0x55CCff, side:THREE.BackSide});
var skybox = new THREE.Mesh(cube, skyboxMaterial);
scene.add(skybox);

//Right-handed coord system
camera.position.set(0, 4, 12);

//Create the renderer
var renderer = new THREE.WebGLRenderer({antialias: true}); //render window size 
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

//prevents screen clipping on resize
window.addEventListener("resize", function(){
  var width = window.innerWidth;
  var height = window.innerHeight;
  renderer.setSize(width, height);
  camera.aspect = width / height;
  camera.updateProjectionMatrix();
});

//Lights
var dirLight = new THREE.DirectionalLight(0xFFFFFF, 2);
scene.add(dirLight);

//PointLight(Color, intensity)
var plRight = new THREE.PointLight(0xffffff, 0);
var plLeft = new THREE.PointLight(0xffffff, 2);
var plFront = new THREE.PointLight(0xffffff, 0);
var plBack = new THREE.PointLight(0xffffff, 2);
var plDigRF = new THREE.PointLight(0xffffff, 1);

plRight.position.set(10, 0, 0);
plLeft.position.set(-10, 0, 0);
plFront.position.set(0, 0, 10);
plBack.position.set(0, 0, -20);
plDigRF.position.set(10, 0, 10);

scene.add(plRight, plLeft, plFront, plBack, plDigRF);


//Controls
controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.autoRotate = false;
controls.autoRotateSpeed = -0.1;
controls.enableKeys = false;
controls.enablePan = false;
controls.maxPolarAngle = Math.PI / 2;
controls.maxDistance = 20;
controls.minDistance = 5;


//Load model
var loader = new THREE.GLTFLoader();
var model;
loader.load('cena.glb', function (gltf){
  model = gltf.scene;
  scene.add(model);  
  model.position.set(0, -1, 0);
}, undefined, function (error){
  console.error(error);
});

//Game logic (runs every frame)
function update() {
  
}


//Draw scene
function render() {
  renderer.render(scene, camera);
}


//Run game loop (update, render, repeat)
function GameLoop() {
  requestAnimationFrame(GameLoop);
  
  controls.update();
  update();
  render();
}
GameLoop();




function tween(obj) {
	var tweenScreenOut = createjs.Tween.get(obj.position, {
		loop: false
	})
		.to({
			x: 60
		}, 1000, createjs.Ease.quadInOut);
}
tween(camera);




